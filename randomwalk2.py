from gturtle import *
from gpanel import *
from math import sqrt

makeTurtle()
makeGPanel(-100, 1100, -10, 110)
windowPosition(850, 10)
drawGrid(0, 1000, 0, 100)
title("Mean distance versus time")
setTitle("Random Walk 2")
hideTurtle()
penUp()

for t in range(100, 1100, 100):
    count = 0
    clean()
    repeat(1000):
        repeat(t):
            forward(2.5)
            setRandomHeading()
        dot(3)
        r = math.sqrt(getX()*getX() + getY()*getY())
        count += r
        home()
    d = count/1000
    print("t = ", t, " d = ", d, " q = d/sqrt(t) = ", d/sqrt(t))
    draw(t, d)
    fillCircle(5)
    delay(2000)
print("I did it, Babe!")