from gturtle import *
from random import randint

codingtrain = ["#f05025", "#f89e50", "#f8ef22", "#31c5f4", "#f063a4",
               "#9252a1", "#817ac6", "#62c777"]

tf = TurtleFrame("Coding Train Dots")
alice = Turtle(tf)
alice.hideTurtle()

for _ in range(3000):
    alice.setPenColor(codingtrain[randint(0, 7)])
    x = randint(-400, 400)
    y = randint(-300, 300)
    alice.setPos(x, y)
    alice.dot(randint(4, 40))

print("I did it, Babe!")

