from gamegrid import *
from random import randint
import os

WIDTH = 640
HEIGHT = 416
NFISHES = 12  # Anzahl der Fische

dt = 20  # delta time

class Fish(Actor):
       
    def __init__(self):
        Actor.__init__(self, True, os.path.join(os.getcwd(), "sprites/fish") + str(randint(1, 7)) + ".png", 2)
        self.speed = randint(-3, 3)
        if self.speed < 0:
            self.setHorzMirror(True)
        elif self.speed == 0:
            self.speed = randint(1, 3)
            
        self.timer = 5
    
    def act(self):
        self.move(self.speed)
        if self.getX() > 600:
            self.setHorzMirror(True)
            self.speed = -randint(1, 3)
        if self.getX() < 40:
            self.setHorzMirror(False)
            self.speed = randint(1, 3)
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 5
        self.timer -= 1

win = GameGrid(WIDTH, HEIGHT, 1, None, os.path.join(os.getcwd(), "sprites/background.png"), False)
win.setBgColor(49, 197, 244)
win.setTitle(u"Jörgs Aquarium")

x = 40
y = 30
for _ in range(NFISHES):
    fish = Fish()
    win.addActor(fish, Location(x, y))
    x += randint(30, 60)
    y += randint(10, 40)

win.setSimulationPeriod(dt)
win.show()
win.doRun()