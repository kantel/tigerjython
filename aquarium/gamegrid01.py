import gamegrid as gg
from random import randint

WIDTH = 640
HEIGHT = 480
NFISHES = 7  # Anzahl der Fische

class Fish(gg.Actor):
    
    def __init__(self, imPath):
        gg.Actor.__init__(self, imPath)
        self.speed = randint(1, 2)
    
    def act(self):
        self.move(self.speed)
        if self.getX() > WIDTH + 30:
            self.reset()
        
    def reset(self):
        self.setX(randint(-270, -30))
        self.setY(randint(30, HEIGHT - 94))

window = gg.GameGrid(WIDTH, HEIGHT, 1, None, "images/background.png", False)
window.setBgColor(161, 214, 231)
window.setTitle("Mein Aquarium")

fishes = []
for _ in range(NFISHES):
    fishes.append(Fish("images/fish" + str(randint(1, 4)) + ".png"))
    window.addActor(fishes[_], gg.Location(randint(-270, -30), randint(30, HEIGHT - 94)))

window.setSimulationPeriod(20)
window.show()
window.doRun()
