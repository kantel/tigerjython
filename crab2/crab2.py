from gamegrid import *
import os

WIDTH = 800
HEIGHT = 560

class Player(Actor):
    pass

def key_pressed(key_code):
    xpos = player.getX()
    if key_code == 37:
        if xpos > 30:
            player.setX(xpos - 5)
    elif key_code == 39:
        if xpos < 770:
            player.setX(xpos + 5)
    win.refresh()

win = GameGrid(WIDTH, HEIGHT)
win.setTitle("Krabbenspiel")
win.setBgColor(49, 197, 244)
player = Player(os.path.join(os.getcwd(), "images/crab2.png"))
win.addActor(player, Location(400, 500))  # (400, 540)
win.addKeyRepeatListener(key_pressed)

win.show()
