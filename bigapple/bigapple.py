from gamegrid import *
from random import randint

WIDTH = 640
HEIGHT = 420

dt = 20  # delta time
numGreenApples = 100
numRedApples = 10

class Player(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, "images/player.png", 2)
        self.speed = 3
        self.timer = 5
        self.score = 0
     
    def collide(self, actor1, actor2):
        if actor2.__class__.__name__ == "GreenApple":
            win.removeActor(actor2)
            self.score += 1
            setStatusText("Punkte: " + str(self.score))
        elif actor2.__class__.__name__ == "RedApple":
            win.removeActor(actor2)
            self.hide()
            win.refresh()
            win.doPause()
        return(0)

class GreenApple(Actor):
    
    def __init__(self):
        Actor.__init__(self, "images/applegreen.png")
        self.speed = 3
    
    def act(self):
        ypos = self.getY() + self.speed
        self.setY(ypos)
        if ypos > (HEIGHT - 32):
            self.setY(randint(-HEIGHT,  -20))
            self.setX(randint(20, WIDTH - 20))
            self.speed = randint(2, 6)

class RedApple(Actor):
    
    def __init__(self):
        Actor.__init__(self, "images/applered.png")
        self.speed = 3
    
    def act(self):
        ypos = self.getY() + self.speed
        self.setY(ypos)
        if ypos > (HEIGHT - 32):
            self.setY(randint(-HEIGHT,  -20))
            self.setX(randint(20, WIDTH - 20))
            self.speed = randint(2, 6)        
        
def onKeyPressed(keyCode):
    xpos = player.getX()
    if keyCode == 37:   #left
        player.timer -= 1
        if player.timer == 0:
            player.showNextSprite()
            player.timer = 5
        player.setHorzMirror(True)
        if xpos > 20:
            player.setX(xpos - player.speed)
    elif keyCode == 39: #right
        player.setHorzMirror(False)
        player.timer -= 1
        if player.timer == 0:
            player.showNextSprite()
            player.timer = 5
        if xpos < (WIDTH - 20):           
            player.setX(xpos + player.speed)

win = makeGameGrid(WIDTH, HEIGHT, 1, None, "images/background.png", False)
win.setBgColor(105, 210, 232)
win.setTitle(u"Zu viele Äpfel")
win.addStatusBar(30)

player = Player()
win.addActor(player, Location(WIDTH//2, HEIGHT - 55))
player.show(1)
win.addKeyRepeatListener(onKeyPressed)

for _ in range(numGreenApples):
    greenApple = GreenApple()
    greenApple.speed = randint(2, 6)
    player.addCollisionActor(greenApple)
    win.addActor(greenApple, Location(randint(20, WIDTH - 20), randint(-HEIGHT, -20)))

for _ in range(numRedApples):
    redApple = RedApple()
    redApple.speed = randint(2, 6)
    player.addCollisionActor(redApple)
    win.addActor(redApple, Location(randint(20, WIDTH - 20), randint(-HEIGHT, -20)))

win.setSimulationPeriod(dt)
win.show()
win.doRun()
