from gturtle import *

def goldstein(t, step, s1, s2, a1, a2):
    for _ in range(step):
        t.forward(s1)
        t.right(a1)
        t.forward(s2)
        t.forward(a2)

step = 37
s1 = 77
a1 = 140.86
s2 = 310
a2 = 112

Options.setPlaygroundSize(600, 600)     # Fenstergröße
tf = TurtleFrame()
tf.setTitle("Goldstein-Figur 3")
tf.clear(makeColor(60, 76, 97))        # Hintergrundfarbe
t = Turtle(tf)
t.setPenColor(makeColor(226, 107, 67))  # Stiftfarbe der Turtle
t.setLineWidth(2)
t.hideTurtle()
t.penUp()
t.setPos(-90, 170)
t.penDown()

goldstein(t, step, s1, s2, a1, a2)
