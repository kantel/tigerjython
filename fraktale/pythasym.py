# Symmetrischer Pythagoras-Baum

from gturtle import *
import math

WIDTH = 640
HEIGHT = 480

palette = [makeColor(200, 23, 223), makeColor(18, 184, 116), makeColor(95, 145, 40),
           makeColor(8, 124, 127)]

def tree(s, level):
    if level < 1:
        return
    else:
        quadrat(s)
        # Linke Seite
        ls = s*math.sqrt(3)/2
        p.forward(s)
        p.left(90)
        p.forward(s)
        p.right(150)
        p.forward(ls)
        p.left(90)
        tree(ls, level - 1)
        # Rechte Seite
        rs = s/2
        p.right(180)
        p.forward(rs)
        p.left(90)
        tree(rs, level - 1)
        p.left(60)
        p.back(s)

def quadrat(s):
    col = palette[int(s - 2)%len(palette)]
    p.setPenColor(col)
    p.setFillColor(col)
    p.startPath()
    for _ in range(4):
        p.forward(s)
        p.left(90)
    p.fillPath()

Options.setPlaygroundSize(WIDTH, HEIGHT)
wn = TurtleFrame()
wn.setTitle("Asymmetrischer Pythagoras-Baum")
wn.clear(makeColor(232, 226, 7)) # Hintergrundfarbe
p = Turtle(wn)
p.hideTurtle()

p.penUp()
p.setPos(120, -HEIGHT/2 + 60)
p.penDown()
tree(85, 14)

print("I did it, Babe!")


