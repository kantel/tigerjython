# Symmetrischer Pythagoras-Baum

from gturtle import *
import math

WIDTH = 640
HEIGHT = 480

palette = [makeColor(189, 183, 110), makeColor(0, 100, 0), makeColor(34, 139, 105),
           makeColor(152, 251, 152), makeColor(85, 107, 47), makeColor(139, 69, 19),
           makeColor(154, 205, 50), makeColor(107, 142, 35), makeColor(139, 134, 78),
           makeColor(139, 115, 85)]

def tree(s):
    if s < 2:
        return
    quadrat(s)
    p.forward(s)
    s1 = s/math.sqrt(2)
    p.left(45)
    tree(s1)
    p.right(90)
    p.forward(s1)
    tree(s1)
    p.back(s1)
    p.left(45)
    p.back(s)

def quadrat(s):
    col = palette[int(s - 2)%len(palette)]
    p.setPenColor(col)
    p.setFillColor(col)
    p.startPath()
    for _ in range(4):
        p.forward(s)
        p.right(90)
    p.fillPath()

Options.setPlaygroundSize(WIDTH, HEIGHT)
wn = TurtleFrame()
wn.setTitle("Symmetrischer Pythagoras-Baum")
wn.clear(makeColor(43, 62, 80)) # Hintergrundfarbe
p = Turtle(wn)
p.hideTurtle()

p.penUp()
p.setPos(-50, -HEIGHT/2 + 50)
p.penDown()
tree(100)

print("I did it, Babe!")


