from gturtle import *

# Konstanten-Deklaration
WIDTH = 400
HEIGHT = 400

# Farben
colors = [(150, 100, 255), (255, 100, 150), (150, 255, 100), (255, 150, 100)]

seiten = 4      # Anzahl der Seiten der Schneeflocke,
                # entweder 3 oder 4
it = 3          # Iterationstiefe, entweder 3 oder 4

Options.setPlaygroundSize(WIDTH, HEIGHT)
wn = TurtleFrame()
wn.setTitle("Kochsche Schneeflocke")
wn.clear(makeColor("#2B3E50"))  # Hintergrundfarbe

def kochkurve(length, d):
    if d == 0:
        koch.forward(length)
    else:
        kochkurve(length/3, d-1)
        koch.left(60)
        kochkurve(length/3, d-1)
        koch.right(120)
        kochkurve(length/3, d-1)
        koch.left(60)
        kochkurve(length/3, d-1)

def schneeflocke(length, d):
    for i in range(seiten):
        koch.setPenColor(makeColor(colors[i%4]))
        kochkurve(length, d)
        koch.right(360/seiten)

koch = Turtle(wn)
koch.hideTurtle()
koch.setPenWidth(1)
koch.penUp()
if seiten == 3:
    koch.setPos(-60, -100)
elif seiten == 4:
    koch.setPos(-100, -100)
koch.penDown()
schneeflocke(200, it)

