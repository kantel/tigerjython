from gturtle import *
import colorsys

Options.setPlaygroundSize(500, 500)  # Fenstergröße
Options.setFramePosition(1300, 50)   # Fensterposition
win = TurtleFrame()
win.setTitle("Colorsys-Test 🐢")     # Mit Turtle-Emoji (im Editor nicht sichtbar)
win.clear(makeColor(54, 50, 80))     # Hintergrundfarbe

alice = Turtle(win)
alice.penWidth(2)
alice.hideTurtle()
alice.speed(-1)

hue = 0.0

for i in range(300):
    color = colorsys.hsv_to_rgb(hue, 1.0, 1.0)
    alice.setPenColor(makeColor(color))
    hue += 0.004
    # print(hue)
    alice.leftCircle(50)
    alice.forward(i)
    alice.left(91)

print("I did it, Babe!")