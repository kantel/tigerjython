from gturtle import *
from random import randint

tf = TurtleFrame()
tf.title = "(Halb-) Transparente Punkte"
alice = Turtle(tf)
alice.hideTurtle()

for i in range(200):
    x = randint(-350, 350)
    y = randint(-250, 250)
    alice.setPos(x, y)
    r = randint(10, 200)
    g = randint(10, 200)
    b = randint(10, 200)
    c = Color(r, g, b)
    ca = makeColor(c, 0.5)    # Alpha = 0.5
    alice.setPenColor(ca)
    d = randint(10, 150)
    alice.dot(d)