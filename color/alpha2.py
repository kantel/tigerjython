from gpanel import *
from random import randint

WIDTH = 600
HEIGHT = 400

makeGPanel(Size(WIDTH, HEIGHT))
window(0, WIDTH, HEIGHT, 0)
windowPosition(410, 150)
bgColor(Color(235, 215, 182)) # Packpapier
title("Random Rectangles with Transparency")

for _ in range(200):
    x = randint(15, WIDTH - 15)
    y = randint(10, HEIGHT - 15)
    w = randint(10, 50)
    h = randint(10, 50)
    move(x, y)
    r = randint(10, 200)
    g = randint(10, 200)
    b = randint(10, 200)
    c = Color(r, g, b)
    ca = makeColor(c, 0.7)    # Alpha = 0.7
    setColor(ca)
    fillRectangle(w, h)

print("I did it, Babe!")
    