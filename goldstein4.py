from gturtle import *

def goldstein(t, step, s1, s2, s3, a1, a2, a3):
    for _ in range(step):
        t.forward(s1)
        t.right(a1)
        t.forward(s2)
        t.forward(a2)
        t.forward(s3)
        t.forward(a3)

step = 47
s1 = 15.4
a1 = 140.86
s2 = 62
a2 = 112
s3 = 57.2
a3 = 130

Options.setPlaygroundSize(500, 500)     # Fenstergröße
tf = TurtleFrame()
tf.setTitle("Goldstein-Figur 4")
tf.clear(makeColor(226, 107, 67))       # Hintergrundfarbe
t = Turtle(tf)
t.setPenColor(makeColor(60, 76, 97))    # Stiftfarbe der Turtle
t.setLineWidth(2)
t.hideTurtle()
t.penUp()
t.setPos(-70, 170)
t.penDown()

goldstein(t, step, s1, s2, s3, a1, a2, a3)
