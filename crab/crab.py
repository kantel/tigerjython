import gamegrid as gg
from random import randint

WIDTH = 640
HEIGHT = 480
NBUBBLES = 100  # Anzahl der Luftblasen

LEFT = 37
RIGHT = 39

class Crab(gg.Actor):

    def __init__(self, imPath):
        gg.Actor.__init__(self, imPath)
    
    def collide(self, player, other):
        window.removeActor(other)
        return(0)

class Bubble(gg.Actor):
    
    def __init__(self, imPath):
        gg.Actor.__init__(self, imPath)

    def act(self):
        ypos = self.getY() + 3
        self.setY(ypos)
        if ypos > HEIGHT + 10:
            self.setY(-10)    

def keyPressed(code):
    xpos = crab.getX()
    if code == LEFT:
        if xpos > 36:
            crab.setX(xpos - 5)
    elif code == RIGHT:
        if xpos < WIDTH - 36:
            crab.setX(xpos + 5)

window = gg.GameGrid(WIDTH, HEIGHT)
window.setBgColor(161, 214, 231)
window.setTitle("Playing Crab")
crab = Crab("sprites/crab.png")
window.addActor(crab, gg.Location(WIDTH//2, HEIGHT - 30))

bubbles = []
for _ in range(NBUBBLES):
    bubbles.append(Bubble("sprites/bubble1.png" ))
    crab.addCollisionActor(bubbles[_])
    window.addActor(bubbles[_], gg.Location(randint(30, WIDTH-30), randint(-(HEIGHT-30), -30)))

window.setSimulationPeriod(20)
window.addKeyRepeatListener(keyPressed)
window.show()
window.doRun()