from gamegrid import *

world = GameGrid(640, 480)
world.setBgColor(color("#94b0c2"))
world.setTitle("TigerJython Hack")

link = Actor("assets/link.png")
world.addActor(link, Location(300, 300))

world.show() 