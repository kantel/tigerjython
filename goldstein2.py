from gturtle import *

def goldstein(t, step, s, a):
    for _ in range(step):
        t.forward(s)
        t.right(a)

step = 142
s = 400
a = 159.72

Options.setPlaygroundSize(500, 500)     # Fenstergröße
tf = TurtleFrame()
tf.setTitle("Goldstein-Figur 2")
tf.clear(makeColor(232, 226, 7))        # Hintergrundfarbe
t = Turtle(tf)
t.setPenColor(makeColor(200, 25, 225))  # Stiftfarbe der Turtle
t.setLineWidth(2)
t.hideTurtle()
t.penUp()
t.setPos(-37, -200)
t.penDown()

goldstein(t, step, s, a)
