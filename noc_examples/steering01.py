# Steering 1

from gpanel import *
from pvector import PVector

WIDTH = 640
HEIGHT = 360
                
class Mover():
    
    def __init__(self):
        self.position = PVector(320, 180)
        self.vel = PVector(2.8, 1.6)
        self.d = 20
        self.color = makeColor(248, 239, 34)
        
    def update(self):
        self.position += self.vel
        if self.position.x <= self.d or self.position.x >= WIDTH - self.d:
            self.vel.x =  -self.vel.x
        if self.position.y <= self.d or self.position.y >= HEIGHT - self.d:
            self.vel.y = -self.vel.y
   
    def draw(self):
        pos(int(self.position.x), int(self.position.y))
        setColor(self.color)
        fillCircle(self.d)
        setColor("black")
        circle(self.d)

def drawBackground():
    setColor(makeColor(146, 82, 161))
    fillRectangle(-2, -2, WIDTH + 2, HEIGHT + 4)

def onCloseClicked():
    keepGoing = False
    print("I did it, Babe!")
    dispose()

def onMouseMoved(x, y):
    mover.position.x = x
    mover.position.y = y

makeGPanel(Size(WIDTH, HEIGHT), closeClicked = onCloseClicked, mouseMoved = onMouseMoved)
window(0, WIDTH, 0, HEIGHT)
title("Steering 1")
enableRepaint(False)

mover = Mover()

keepGoing = True
while keepGoing:
    drawBackground()
    mover.draw()
    # mover.update()
    repaint()
    delay(30)
 