from gpanel import *
from math import sqrt, radians, sin, cos

WIDTH = 640
HEIGHT = 400
dt = 30  # delay time


def drawTriangle(x, y):
    setColor("blue")
    fillTriangle(x, y + 50, x - 25, y - 25, x + 25, y - 25)
    

def drawBackground():
    setColor(makeColor(234, 218, 184))
    fillRectangle(-2, -2, WIDTH + 2, HEIGHT + 4)

def onCloseClicked():
    keepGoing = False
    print("I did it, Babe!")
    dispose()

makeGPanel(Size(WIDTH, HEIGHT), closeClicked = onCloseClicked)
window(0, WIDTH, 0, HEIGHT)
title("Bouncing Ball")
enableRepaint(False)

keepGoing = True
while keepGoing: 
    drawBackground()
    drawTriangle(WIDTH/2, HEIGHT/2)
    repaint()
    delay(dt)