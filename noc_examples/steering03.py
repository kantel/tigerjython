from gamegrid import *

WIDTH = 640
HEIGHT = 360

class Target(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/peg_3.png")
        self.location = Location(int(WIDTH/2), int(HEIGHT/2)) # Startposition
        
    def update(self, x, y):
        self.location(x, y)
        
    def draw(self):
        bg.clear()
        bg.fillCircle(self.location, 20)
    
target = Target()

def onMouseMoved(e):
    target.update(e.getX(), e.getY())
    target.draw()
    
win = makeGameGrid(WIDTH, HEIGHT, 1, False, mouseMoved = onMouseMoved)
win.setBgColor(makeColor(146, 82, 161))
win.setTitle("Steering 3")

setSimulationPeriod(5)

win.show()
win.doRun()
bg = getBg()