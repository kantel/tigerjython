# Steering 1

from gpanel import *
from pvector import PVector
import random

WIDTH = 640
HEIGHT = 400
dt = 30     # delay time
                
class Mover():
    
    def __init__(self):
        self.d = random.randint(16, 24)
        self.vel = PVector(1, 1)
        self.vel.mult(random.uniform(2.0, 6.0))
        self.direction = PVector(random.uniform(-1.5, 1.5), random.uniform(-1.5, 1.5))
        self.position = PVector(random.randint(2*self.d, WIDTH - 2*self.d), random.randint(2*self.d, HEIGHT - 2*self.d))
        self.red = random.randint(100, 255)
        self.green = random.randint(0, 150)
        self.blue = random.randint(0, 255)
        
    def update(self):
        self.position.x += self.vel.x*self.direction.x
        self.position.y += self.vel.y*self.direction.y
   
    def draw(self):
        pos(int(self.position.x), int(self.position.y))
        setColor(makeColor(self.red, self.green, self.blue))
        fillCircle(self.d)
        setColor("black")
        circle(self.d)
    
    def checkEdges(self):
        if self.position.x <= self.d or self.position.x >= WIDTH - self.d:
            self.vel.x =  -self.vel.x
        if self.position.y <= self.d or self.position.y >= HEIGHT - self.d:
            self.vel.y = -self.vel.y


def drawBackground():
    setColor(makeColor(234, 218, 184))
    fillRectangle(-2, -2, WIDTH + 2, HEIGHT + 4)

def onCloseClicked():
    keepGoing = False
    print("I did it, Babe!")
    dispose()

makeGPanel(Size(WIDTH, HEIGHT), closeClicked = onCloseClicked)
window(0, WIDTH, 0, HEIGHT)
title("Bouncing Ball")
enableRepaint(False)

movers = []
for _ in range(30):
    movers.append(Mover())

keepGoing = True
while keepGoing:
    drawBackground()
    for mover in movers:
        mover.draw()
        mover.checkEdges()
        mover.update()
    repaint()
    delay(dt)
 