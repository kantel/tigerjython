from gamegrid import *
from random import randint, choice

WIDTH = 600
HEIGHT = 400

dt = 20  # delta time

class Fish(Actor):
       
    def __init__(self):
        Actor.__init__(self, True, "sprites/babelfish.gif")
        self.speed = randint(1, 5)
    
    def act(self):
        self.move(self.speed)
        if self.getX() > 570:
            self.setHorzMirror(True)
            self.speed *= -1
        if self.getX() < 30:
            self.setHorzMirror(False)
            self.speed *= -1

win = GameGrid(WIDTH, HEIGHT, 1, None, "sprites/reef.gif", False)
win.setTitle(u"Jörgs Aquarium")

x = 50
y = 100
for _ in range(5):
    fish = Fish()
    win.addActor(fish, Location(x, y))
    x += randint(50, 100)
    y += randint(50, 100)

win.setSimulationPeriod(dt)
win.show()
win.doRun()

