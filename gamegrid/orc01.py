from gamegrid import *

ts = 32 # Tilesize
level01 = [[1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
           [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
           [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
           [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1],
           [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1]]

class Orc(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/orc.gif", 8)
        self.dir = "right"   # Spieler startet mit Blickrichtung rechts
        self.walking = False # Spieler bewegt sich erst auf Tastendruck
        self.count = 0

class Tree(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/tree.png")
        
        

def keyCallback(e):
    keyCode = e.getKeyCode()
    if keyCode == 37: # left
        orc.setDirection(180)
        orc.show(5)
    elif keyCode == 38: # up
        orc.setDirection(270)
        orc.show(0)
    elif keyCode == 39: # right
        orc.setDirection(0)
        orc.show(7)
    elif keyCode == 40: #down
        orc.setDirection(90)
        orc.show(2)
    # Kollsionserkennung
    nextpos = orc.getNextMoveLocation()
    i = nextpos.getX()
    # Ränderabfrage
    if i == 16:
        i = 0
    elif i == 0:
        i = 15
    j = nextpos.getY()
    if j == 11:
        j = 0
    elif j == 0:
        j = 10
    # print i, j
    # Wenn an der nächsten Position kein Baum, dann gehe dorthin.
    if level01[j][i] != 1:
        orc.move()
        # Ränderbehandlung für den nächsten Schritt
        if orc.getX() < 0:
            orc.setX(15)
        elif orc.getX() > 15:
            orc.setX(0)
        if orc.getY() < 0:
            orc.setY(10)
        elif orc.getY() > 10:
            orc.setY(0)

makeGameGrid(16, 11, 32, None, "sprites/bg0.png", False, keyPressed = keyCallback)
setTitle("Walking Orc")

# Baue den Level auf:
for i in range(len(level01)):
    for j in range(len(level01[i])):
        if level01[i][j] == 1:
            tree = Tree()
            addActor(tree, Location(j, i))
            tree.show()

orc = Orc()
addActor(orc, Location(10, 9))
orc.show(7)

show()
doRun()