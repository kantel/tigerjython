# title: Pumpkin Attack
# Author: Jörg Kantel

from gamegrid import *
from random import randint
import os

WIDTH = 680
HEIGHT = 680
LEFTMARGIN = 40 + 14
RIGHTMARGIN = 640 - 14
TOPMARGIN = 40 + 14
BOTTOMMARGIN = 640 - 14

NOPUMP = 10

DATAPATH = os.path.join(os.getcwd(), "data")

class Background(Actor):
    
    def __init__(self):
        Actor.__init__(self, False, os.path.join(DATAPATH, "moonnight2.jpg"))
        
class Player(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "rocket1.png"))
        

class Enemy(Actor):
    
    def __init__(self):
        Actor.__init__(self, False, os.path.join(DATAPATH, "pumpkin.png"))
        self.speed = randint(1, 5)
        
    def act(self):
        self.move(self.speed)
        if self.getX() >= RIGHTMARGIN:
            self.setX(RIGHTMARGIN)
            self.direction -= randint(90, 270)
        elif self.getX() <= LEFTMARGIN:
            self.setX(LEFTMARGIN)
            self.direction -= randint(90, 270)
        if self.getY() >= BOTTOMMARGIN:
            self.setY(BOTTOMMARGIN)
            self.direction -= randint(90, 270)
        elif self.getY() <= TOPMARGIN:
            self.setY(TOPMARGIN)
            self.direction -= randint(90, 270)

 
win = makeGameGrid(WIDTH, HEIGHT, 1, Color(color("#2b3e50")), True)
win.setPosition(1300, 40)
win.setTitle("Pumpkin Attack, Stage 1 🎃")   # Titelzeile mit Pumpkin-Emoji (im Editor nicht sichtbar)
win.setSimulationPeriod(20)

bg = Background()
win.addActor(bg, Location(WIDTH//2, HEIGHT//2))

for _ in range(NOPUMP):
    p = Enemy()
    win.addActor(p, Location(randint(54, 640 - 14), (randint(54, 640 - 14))))
    p.setDirection(randint(20, 340))

player = Player()
win.addActor(player, Location(320, 320))

win.show()
win.doRun()
