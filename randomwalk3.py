from gturtle import *
from random import randint

WIDTH, HEIGHT = 600, 400

setPlaygroundSize(WIDTH, HEIGHT)
tf = TurtleFrame()
tf.title = "Random Walk"
tf.clean(Color(235, 215, 182)) # Packpapier
alice = Turtle(tf)
alice.hideTurtle()

r = randint(20, 200)
g = randint(20, 200)
b = randint(20, 200)
c = Color(r, g, b)

for _ in range(200000):
    alice.forward(2.5)
    alice.setRandomHeading()
    
    r += randint(-1, 1)
    if r <= 20: r = 200
    if r >= 200: r = 20
    g += randint(-1, 1)
    if g <= 20: g = 200
    if g >= 200: g = 20
    b += randint(-1, 1)
    if b <= 20: b = 200
    if b >= 200: b = 20
    c = Color(r, g, b)
    ca = makeColor(c, 0.4) # alpha
    alice.setPenColor(ca)
    alice.wrap()
    alice.dot(5)

print("I did it, Babe!")
