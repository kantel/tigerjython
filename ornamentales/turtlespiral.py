from gturtle import *

# Konstanten Deklaration
WIDTH = 640
HEIGHT = 480

# Farben
colors = [(150, 100, 255), (255, 100, 150), (150, 255, 100), (255, 150, 100)]

start_length = 5
increment = 5

Options.setPlaygroundSize(WIDTH, HEIGHT)
wn = TurtleFrame()
wn.setTitle("Turtle Spirale")
wn.clear(makeColor((234, 218, 184)))

def square(turtle, side_length):
    for _ in range(4):
        turtle.forward(side_length)
        turtle.right(90)

def spiral(turtle, start_length):
    side_length = start_length
    for i in range(60):
        turtle.setPenColor(makeColor(colors[i%4]))
        square(turtle, side_length)
        side_length += increment
        turtle.right(increment)

alex = Turtle(wn)
alex.hideTurtle()
alex.penUp()
alex.setPos(-90, -105)
alex.right(90)
alex.setPenWidth(2)
alex.penDown()
spiral(alex, start_length)
print("I did it, Babe!")
    