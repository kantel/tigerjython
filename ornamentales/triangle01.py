from gpanel import *
from math import sqrt, radians, sin, cos
import time
from random import randint, random

WIDTH = 100
HEIGHT = 100

class Star():
    
    def __init__(self, size):
        self.size = size
        self.direction = 0
        self.color = "yellow"
        self.triangle1 = [[-sqrt(3)/2*self.size, -self.size/2],
                          [sqrt(3)/2*self.size, -self.size/2], [0, self.size]]
        self.triangle2 = [[-sqrt(3)/2*self.size, self.size/2],
                          [sqrt(3)/2*self.size, self.size/2], [0, -self.size]]
        
    def setColor(self, color):
        self.color = color
    
    def getDirection(self):
        return(self.direction)
    
    def rotate(self, angle):
        self.direction += angle
        self.triangle1 = self._rotatePolygon(self.triangle1, angle)
        self.triangle2 = self._rotatePolygon(self.triangle2, angle)
        
    def draw(self, position):
        setColor(self.color)
        fillPolygon(self._translatePolygon(self.triangle1, position[0], position[1]))
        fillPolygon(self._translatePolygon(self.triangle2, position[0], position[1]))
    
    def _translatePolygon(self, polygon, x, y):
        translatedPolygon = []
        for corner in polygon:
            translatedPolygon.append([corner[0] + x, corner[1] + y])
        return(translatedPolygon)
    
    def _rotatePolygon(self, polygon, theta):
        theta = radians(theta)
        rotatedPolygon = []
        for corner in polygon:
            rotatedPolygon.append([corner[0]*cos(theta) - corner[1]*sin(theta),
                                   corner[0]*sin(theta) + corner[1]*cos(theta)])
        return(rotatedPolygon)

dt = 0.01
makeGPanel(0, 100, 0, 100)
bgColor("blue")
clear()

nbStars = 5
stars = [0]*nbStars
for n in range(nbStars):
    stars[n] = Star(randint(3, 8))
for n in range(nbStars):
    stars[n].setColor(makeColor(randint(0, 255), randint(0, 255), randint(0, 255)))

positions = [0]*nbStars
for n in range(nbStars):
    positions[n] = [randint(0, 100), randint(0, 100)]

rotIncrement = [0]*nbStars
for n in range(nbStars):
    rotIncrement[n] = (-2 + 4*random())

enableRepaint(False)
while True:
    clear()
    positionTemp = []
    for n in range(nbStars):
        stars[n].rotate(rotIncrement[n])
        stars[n].draw(positions[n])
        if positions[n][1] < -5:
            positions[n][1] =  105
        else:
            positions[n][1] -= 0.1
        positionTemp.append([positions[n][0], positions[n][1]])
        position = positionTemp
        repaint()
        time.sleep(dt)
        
    