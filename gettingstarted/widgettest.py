from gpanel import *
from javax.swing import *

WIDTH = 640
HEIGTH = 480

makeGPanel(Size(WIDTH, HEIGTH))
window(-10, 110, -10, 110)
windowPosition(1400, 80)
bgColor("white")
title("Widget Test")

lbl1 = JLabel("Anzahl Kreise")
txf1 = JTextField(10)
addComponent(lbl1)
addComponent(txf1)
# addStatusBar(22)
# setStatusText("Hallo Welt")

setColor(Color(230, 226, 204))
fillRectangle(0, 0, 100, 100)
setColor("black")
rectangle(0, 0, 100, 100)
