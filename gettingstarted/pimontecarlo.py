# PI – Monte Carlo Simulation

from gpanel import *
from javax.swing import *
from random import random

WIDTH = 600
HEIGTH = 600

def actionCallback(e):
    wakeUp()

def createGUI():
    addComponent(lbl1)
    addComponent(tf1)
    addComponent(btn1)
    addComponent(lbl2)
    addComponent(tf2)
    validate()
    
def init():
    tf2.setText("")
    clear()
    setColor(230, 226, 204)
    fillRectangle(0, 0, 1, 1)
    setColor(4, 21, 31)
    rectangle(0, 0, 1, 1)
    arc(1, 0, 90)

def doIt(n):
    hits = 0
    for _ in range(n):
        zx = random()
        zy = random()
        if zx*zx + zy*zy < 1:
            hits += 1
            setColor(226, 107, 67)
        else:
            setColor(60, 76, 97)
        # point(zx, zy)
        move(zx, zy)
        fillCircle(0.002)
    return(hits)

lbl1 = JLabel("Anzahl der Punkte: ")
lbl2 = JLabel("PI = ")
tf1 = JTextField(6)
tf2 = JTextField(10)
btn1 = JButton("OK", actionListener = actionCallback)


makeGPanel(Size(WIDTH, HEIGTH))
window(-0.1, 1.1, -0.1, 1.1)
windowPosition(1400, 80)
bgColor(240, 245, 248)
title("Pi via Monte Carlo Simulation")

createGUI()
tf1.setText("200000")
init()

while True:
    putSleep()
    init()
    n = int(tf1.getText())
    k = doIt(n)
    pi = 4*k/n
    tf2.setText(str(pi))