# Slider-Demo

from gpanel import *

WIDTH = 600
HEIGTH = 600

def f(x, a, b, c):
    return(a*(x - b)**2 + c)

def doIt():
    clear()
    setColor(230, 226, 204)
    fillRectangle(-5, -10, 5, 10)
    drawGrid(-5, 5, -10, 10, 4, 4, "darkgrey")
    a = sldA.getValue()
    b = sldB.getValue()
    c = sldC.getValue()
    setColor("blue")
    x = -5
    candraw = False
    lineWidth(2)
    while x < 5:
        y = f(x, a, b, c)
        if x == -5 or abs(y) >= 9.9:
            move(x, y)
        else:
            if abs(y) < 10:
                draw(x, y)
        x += 0.01
    status.setValue("Drücke »Go« für neue Berechnung")

makeGPanel(Size(WIDTH, HEIGTH))
window(-6.5, 5.5, -12, 11)
windowPosition(1400, 80)
bgColor(240, 245, 248)
title("Slider Demo")
setColor(230, 226, 204)
fillRectangle(-5, -10, 5, 10)
drawGrid(-5, 5, -10, 10, 4, 4, "darkgrey")

sldA = SliderEntry(-2, 2, 1, 1, 0) # (min, max, init, Abstand Hauptticks, Abstand Zwischenticks)
pane1 = EntryPane("a", sldA)
sldB = SliderEntry(-5, 5, 1, 2, 1)
pane2 = EntryPane("b", sldB)
sldC= SliderEntry(-10, 10, -10, 5, 1)
pane3 = EntryPane("c", sldC)
status = StringEntry("")
pane4 = EntryPane("Status", status)
startBtn = ButtonEntry("Go")
pane5 = EntryPane(startBtn)

dlg = EntryDialog(2000, 80,
                  pane1, pane2, pane3, pane4, pane5)

status.setValue("Drücke »Go« für den Start")
while not dlg.isDisposed():
    if isDisposed():
        dlg.dispose()
        break
    if startBtn.isTouched():
        doIt()
print("I did it, Babe!")
dispose()

 
doIt()   