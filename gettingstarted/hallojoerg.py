from gpanel import *
from random import randint

WIDTH = 640
HEIGTH = 480

malewitsch1 = [Color(42, 40, 45), Color(54, 50, 80),
               Color(50, 80, 105), Color(160, 51, 46),
               Color(180, 144, 55), Color(140, 82, 48),
               Color(215, 158, 40)]

makeGPanel(Size(WIDTH, HEIGTH))
window(-10, 110, -10, 110)
windowPosition(1400, 80)
bgColor(Color(235, 215, 182))    # Packpapier
title("Random Circles")

for _ in range(200):
    c = (malewitsch1[(randint(0, len(malewitsch1) - 1))])
    ca = makeColor(c, 0.9)       # alpha = 0.9
    setColor(ca)
    pos(randint(0, 100), randint(0, 100))
    fillCircle(1)
    c = (malewitsch1[(randint(0, len(malewitsch1) - 1))])
    ca = makeColor(c, 0.5)       # alpha = 0.5
    setColor(ca)
    fillCircle(randint(2, 7))
    delay(50)

print("I did it, Babe!")
