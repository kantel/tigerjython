from gpanel import *
from math import sin, cos

def f(x):
    if x == 0:
        y = 1
    else:
        y = sin(x)/x
    return(y)



makeGPanel(-12, 11, -1.8, 1.8)
title("Funktionsgraph")
drawGrid(-10, 10, -1.5, 1.5, "gray")

lineWidth(2)

x = -10
while x < 10:
    y = f(x)
    setColor("blue")
    point(x, y)
    y = cos(x)
    setColor("red")
    point(x, y)
    x += 0.01
    
keep()