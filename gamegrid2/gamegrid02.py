from gamegrid import *
from random import randint
import os

# ------------------------ class Player -----------------------

class Player(Actor):
    
    def __init__(self):
        Actor.__init__(self, os.path.join(os.getcwd(), "images/player.png"))
    
    def act(self):
        pass
        

screen = GameGrid(30, 30, 16, Color.red, os.path.join(os.getcwd(), "images/map.png"), False)
screen.setTitle("Jörgs kleine Welt")

player = Player()

screen.addActor(player, Location(14, 15), 0)

screen.show()