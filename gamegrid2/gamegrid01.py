from gamegrid import *
from random import randint

# --------------------- class Alien ---------------------

class Alien(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/alien.png")
    
    def act(self):
        self.move()
        if self.getY() >= 10:
            self.setY(-randint(0, 3))
            self.setX(randint(0, 9))

screen = GameGrid(10, 10, 60, Color.red, "sprites/town.jpg", False)
screen.setTitle("GameGrid-Fenster")

ajax = Alien()
achill = Alien()

screen.addActor(ajax, Location(randint(0, 9), 0), 90)
screen.addActor(achill, Location(randint(0, 9), 0), 90)

screen.show()
screen.doRun()