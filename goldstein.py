from gturtle import *

def goldstein(t, step, s, a):
    for _ in range(step):
        t.forward(s)
        t.right(a)

step = 31
s = 300
a = 151

Options.setPlaygroundSize(400, 400)     # Fenstergröße
tf = TurtleFrame()
tf.setTitle("Goldstein-Figur 1")
tf.clear(makeColor(232, 226, 7))        # Hintergrundfarbe
t = Turtle(tf)
t.setPenColor(makeColor(200, 25, 225))  # Stiftfarbe der Turtle
t.setLineWidth(2)
t.hideTurtle()
t.penUp()
t.setPos(-40, -150)
t.penDown()

goldstein(t, step, s, a)
