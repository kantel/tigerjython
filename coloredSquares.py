import gpanel as g

SIZE = 10

def drawStone(x, y):
    g.setColor(g.getRandomX11Color())
    g.move(x + SIZE/2, y + SIZE/2)
    g.fillRectangle(SIZE, SIZE)

g.makeGPanel(0, 400, 0, 400)
g.title("Little Squares")

for x in range(0, 400, SIZE):
    for y in range(0, 400, SIZE):
        drawStone(x, y)

print("I did it, Babe!")