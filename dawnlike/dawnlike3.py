from gamegrid import *

ts = 16 # Tilesize
width = 25
height = 24

level01 = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1],
           [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1],
           [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
           [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
           [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
           [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
           [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
           [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
           [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
           [1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]


class Player(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/hero.png", 2)
        self.dir = "right" # Spieler startet mit Blickrichtung nach rechts
    
    def act(self):
        self.showNextSprite()
        
class Enemy(Actor):
    
    def __init__(self, spritepath, startLoc):
        Actor.__init__(self, spritepath, 2)
        self.dir = "right" # Auch die Gegner starten mit Blickrichtung nach rechts
        self.startLocation = startLoc
    
    def act(self):
        self.showNextSprite()

def enemyMove():
    pass

def keyCallback(e):
    keyCode = e.getKeyCode()
    # print(keyCode)
    if keyCode == 37: # left
        player.setDirection(180)
        player.show(1)
    elif keyCode == 38: # up
        player.setDirection(270)
        player.show(3)
    elif keyCode == 39: # right
        player.setDirection(0)
        player.show(2)
    elif keyCode == 40: #down
        player.setDirection(90)
        player.show(0)
    # Kollsionserkennung
    nextpos = player.getNextMoveLocation()
    i = nextpos.getX()
    # Ränderabfrage
    if i == width:
        i = 0
    elif i == 0:
        i = width - 1
    j = nextpos.getY()
    if j == height:
        j = 0
    elif j == 0:
        j = height - 1
    # Wenn an der nächsten Position kein Hindernis, dann gehe dorthin.
    if level01[j][i] != 1:
        player.move()
        # Ränderbehandlung für den nächsten Schritt
        if player.getX() < 0:
            player.setX(width - 1)
        elif player.getX() > width - 1:
            player.setX(0)
        if player.getY() < 2:
            player.setY(height - 3)
        elif player.getY() > height - 3:
            player.setY(2)
    for enemy in enemies:
        enemyMove()
        
scrn1 = makeGameGrid(width, height, ts, None, "sprites/map01.png", False, keyPressed = keyCallback)
scrn1.setTitle("DawnHack")

scrn2 = GameGrid(width, height, ts, None, "sprites/map01.png", False, keyPressed = keyCallback)
scrn2.setTitle("DawnHack 2")

enemies = []
zombie = Enemy("sprites/zombie.png", Location(7, 11))
zombie2 = Enemy("sprites/zombie.png", Location(5, 13))
enemies.append(zombie)

scrn1.addActor(zombie, zombie.startLocation)
zombie.show()
scrn2.addActor(zombie2, zombie2.startLocation)
zombie2.show()


player = Player()
scrn1.addActor(player, Location(14, 14))
player.show()

player2 = Player()
scrn2.addActor(player2, Location(18, 12))
player2.show()

scrn1.show()
scrn2.show()
scrn1.doRun()
scrn2.doRun()