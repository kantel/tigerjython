from gamegrid import *

win = makeGameGrid(600, 600, 1, None, "sprites/town.jpg", True)
win.setTitle("Alien Attack Stage 2")
win.setSimulationPeriod(20)

class SpaceShip(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/spaceship.gif")
        self.timer = 0
        
    def act(self):
        self.timer -= 1
    

class Missile(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/bomb.gif")
    
    def act(self):
        ypos = self.getY()
        self.setY(ypos - 5)
        if ypos < 0:
            win.removeActor(self)
    
    def collide(self, actor1, actor2):
        xpos = actor2.getX()
        ypos = actor2.getY()
        win.removeActor(self)
        win.removeActor(actor2)
        hit = Explosion()
        win.addActor(hit, Location(xpos, ypos))
        return(0)
            
class Alien(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/alien.gif", 2)
        self.ypos = 0
        self.timer = 10
    
    def act(self):
        self.ypos += 0.1
        self.setY(int(self.ypos))
        self.timer -= 1
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 10
            
class Explosion(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/hit.gif")
        self.timer = 5
    
    def act(self):
        self.timer -= 1
        if self.timer == 0:
            win.removeActor(self)

def fire():
    if player.timer < 0:
        missile = Missile()
        aliens = win.getActors(Alien)
        for alien in aliens:
            missile.addCollisionActor(alien)
        win.addActor(missile, Location(player.getX(), 550))
        player.timer = 10
    
def keyCallback(keyCode):
    xpos = player.getX()
    if keyCode == 37: #left
        if xpos > 20:
            player.setX(xpos - 5)
    elif keyCode == 39: #right
        if xpos < 580:
            player.setX(xpos + 5)
    elif keyCode == 32: #space
        fire()

for row in range(50, 300, 50):
    for col in range(40, 570, 40):
        alien = Alien()
        alien.ypos = row
        win.addActor(alien, Location(col, row))

player = SpaceShip()
win.addActor(player, Location(300, 560))
win.addKeyRepeatListener(keyCallback)

win.show()
win.doRun()