from gamegrid import *
from random import randint

win = makeGameGrid(600, 600, 1, None, "sprites/town.jpg", False )
win.setTitle("Alien Attack Stage 3")
win.setSimulationPeriod(20)

class SpaceShip(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/spaceship.gif")
        self.timer = 0
        
    def act(self):
        self.timer -= 1
    

class Missile(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/bomb.gif")
        self.speed = 5
    
    def act(self):
        ypos = self.getY()
        self.setY(ypos - self.speed)
        if ypos < 0:
            win.removeActor(self)
    
    def collide(self, actor1, actor2):
        xpos = actor2.getX()
        ypos = actor2.getY()
        win.removeActor(self)
        win.removeActor(actor2)
        hit = Explosion()
        win.addActor(hit, Location(xpos, ypos))
        if win.getNumberOfActors(Alien) == 0:
            winner()
        return(0)
            
class Alien(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/alien.gif", 2)
        self.ypos = 0
        self.timer = 10
        self.speed = 0.1
    
    def act(self):
        self.ypos += self.speed
        self.setY(int(self.ypos))
        self.timer -= 1
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 10
        if randint(1, 1000) == 500:
            bomb = Bomb()
            bomb.addCollisionActor(player)
            win.addActor(bomb, Location(self.getX(), self.getY() + 10))
        if self.ypos > 580:
            gameover()

class Bomb(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/creature.gif", 2)
        self.timer = 10
        self.speed = 5
    
    def act(self):
        self.timer -= 1
        ypos = self.getY()
        self.setY(ypos + self.speed)
        if ypos > 600:
            win.removeActor(self)
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 10
    
    def collide(self, actor1, actor2):
        gameover()
        return(0)
            
class Explosion(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/hit.gif")
        self.timer = 5
    
    def act(self):
        self.timer -= 1
        if self.timer == 0:
            win.removeActor(self)

def fire():
    if player.timer < 0:
        missile = Missile()
        aliens = win.getActors(Alien)
        for alien in aliens:
            missile.addCollisionActor(alien)
        bombs = win.getActors(Bomb)
        for bomb in bombs:
            missile.addCollisionActor(bomb)
        win.addActor(missile, Location(player.getX(), 550))
        player.timer = 10
    
def keyCallback(keyCode):
    xpos = player.getX()
    if keyCode == 37: #left
        if xpos > 20:
            player.setX(xpos - 5)
    elif keyCode == 39: #right
        if xpos < 580:
            player.setX(xpos + 5)
    elif keyCode == 32: #space
        fire()

def gameover():
    hit = Explosion()
    win.addActor(hit, Location(player.getX(), 565))
    win.removeActor(player)
    youLost = Actor("sprites/gameover.gif")
    win.addActor(youLost, Location(300, 300))
    win.doPause()
 
def winner():
    youWin = Actor("sprites/you_win.gif")
    win.addActor(youWin, Location(300, 300))
    win.doPause()

for row in range(50, 300, 50):
    for col in range(40, 570, 40):
        alien = Alien()
        alien.ypos = row
        win.addActor(alien, Location(col, row))

player = SpaceShip()
win.addActor(player, Location(300, 560))
win.addKeyRepeatListener(keyCallback)

win.show()
win.doRun()