from gamegrid import *

win = makeGameGrid(600, 600, 1, None, "sprites/town.jpg", True)
win.setTitle("Alien Attack")
win.setSimulationPeriod(20)

class SpaceShip(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/spaceship.gif")
        self.timer = 0
        
    def act(self):
        self.timer -= 1
    

class Missile(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/bomb.gif")
    
    def act(self):
        ypos = self.getY()
        self.setY(ypos - 5)
        if ypos < 0:
            win.removeActor(self)

def fire():
    if player.timer < 0:
        missile = Missile()
        win.addActor(missile, Location(player.getX(), 550))
        player.timer = 10
    
def keyCallback(keyCode):
    xpos = player.getX()
    if keyCode == 37: #left
        if xpos > 20:
            player.setX(xpos - 5)
    elif keyCode == 39: #right
        if xpos < 580:
            player.setX(xpos + 5)
    elif keyCode == 32: #space
        fire()

player = SpaceShip()
win.addActor(player, Location(300, 560))
win.addKeyRepeatListener(keyCallback)

win.show()
win.doRun()