import gpanel as g

SIZE = 10
WW = 300
WH = 300

@g.onMousePressed
def doIt(x, y):
    g.move(x + SIZE/2, y + SIZE/2)
    g.fillCircle(SIZE)
    print(x + SIZE/2, y + SIZE/2)

g.makeGPanel(g.Size(WW, WH))
g.title("Mein kleines Testfensterchen")
g.window(0, WW, WH, 0)
g.resizable(False)
g.bgColor(g.makeColor(120, 120, 120))
g.setColor(g.makeColor(0, 200, 0))
myFont = g.Font("Courier New", g.Font.PLAIN, 32)
g.font(myFont)
g.text(10, 200, "Let's Do It!")
g.setColor(g.makeColor(200, 0, 0))