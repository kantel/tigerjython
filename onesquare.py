import gpanel as g

SIZE = 40

def drawStone(x, y):
    g.setColor(g.getRandomX11Color())
    g.move(x + SIZE/2, y + SIZE/2)
    g.fillRectangle(SIZE, SIZE)

g.makeGPanel(0, 400, 400, 0)
g.title("Square")

drawStone(0, 0)

print("I did it, Babe!")