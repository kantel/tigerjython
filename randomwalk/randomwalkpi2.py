from gturtle import *
from random import randint

WIDTH, HEIGHT = 640, 480

setPlaygroundSize(WIDTH, HEIGHT)
playground = TurtleFrame()
playground.title = "Random Walk with Pi (2)"
playground.clean(Color(235, 215, 182)) # Packpapier

pina = Turtle(playground)
pina.hideTurtle()
pina.setLineWidth(3)
# pina.right(90)

pidata = open("data/first5000digitsofpi.txt")
s = pidata.read()

digits = list(str(s))
digits = [int(i) for i in digits if i != "."]
for m in digits:
    r = randint(20, 200)
    g = randint(20, 200)
    b = randint(20, 200)
    c = Color(r, g, b)
    ca = makeColor(c, 0.7) # alpha
    pina.setPenColor(ca)
    stepl = randint(0, 100)
    if stepl >= 2:
        step = 10
    else:
        step = 100
    if m == 0 or m == 3 or m == 6 or m == 9:
        pina.right(60)
        pina.forward(step)
    if m == 1 or m == 4 or m == 7:
        pina.right(120)
        pina.forward(step)
    if m == 2 or m == 5 or m == 8:
        pina.right(0)
        pina.forward(step)    
    pina.wrap()


print("I did it, Babe!")