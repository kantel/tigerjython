from gturtle import *
from random import randint

WIDTH, HEIGHT = 640, 480

setPlaygroundSize(WIDTH, HEIGHT)
playground = TurtleFrame()
playground.title = "Random Walk with Pi"
playground.clean(Color(235, 215, 182)) # Packpapier

pina = Turtle(playground)
pina.hideTurtle()
pina.setLineWidth(3)

pidata = open("data/first5000digitsofpi.txt")
s = pidata.read()

digits = list(str(s))
digits = [int(i) for i in digits if i != "."]
for m in digits:
    r = randint(20, 200)
    g = randint(20, 200)
    b = randint(20, 200)
    c = Color(r, g, b)
    ca = makeColor(c, 0.7) # alpha
    pina.setPenColor(ca)
    if m == 0:
         pina.forward(10)
    if m == 1:
        pina.right(90)
        pina.forward(10)
    if m == 2:
        pina.left(90)
        pina.forward(10)
    if m == 3:
        pina.left(180)
        pina.forward(10)
    if m == 4:
        pina.right(270)
        pina.forward(10)
    if m == 5:
        pina.right(45)
        pina.forward(10)
    if m == 6:
        pina.left(45)
        pina.forward(100)
    if m == 7:
        pina.right(60)
        pina.forward(10)
    if m == 8:
        pina.left(60)
        pina.forward(10)
    if m == 9:
        pina.left(30)
        pina.forward(10)
    
    pina.wrap()


print("I did it, Babe!")