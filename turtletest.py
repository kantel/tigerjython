from gturtle import *

makeTurtle()
distance = 10

hideTurtle()
setPenColor("#f05025")
setPenWidth(2)

for _ in range(50):
    forward(distance)
    right(90)
    distance += 10
    
# home()
# showTurtle()
print("I did it, Babe!")