## Stage 1

from gamegrid import *

## Konstanten

LEFT_KEY  = 37
UP_KEY    = 38
RIGHT_KEY = 39
DOWN_KEY  = 40

RIGHT = 0
DOWN  = 90
LEFT  = 180
UP    = 270

## Klassen-Definitionen

class Player(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/player.gif")
    
    def act(self):
        pass
        

class Wall(Actor):
    
    def __init__(self):
        Actor.__init__(self, "sprites/wall.gif")

## Funktionen

def keyCallback(e):
    keyCode = e.getKeyCode()
    if keyCode == LEFT_KEY:
        player.setDirection(LEFT)
#        next = player.getLocation().getNeighbourLocation(LEFT)
#        print(next)
#        if level_1[next.getX()][next.getY()] != "#":
#            player.move()
        player.move()
    elif keyCode == UP_KEY:
        player.setDirection(UP)
        player.move()
    elif keyCode == RIGHT_KEY:
        player.setDirection(RIGHT)
        player.move()
    elif keyCode == DOWN_KEY:
        player.setDirection(DOWN)
        player.move()

## Level Setup

levels = []

level_1 = [
    "####################",
    "# @#               #",
    "#  #######  #####  #",
    "#        #  #      #",
    "#        #  #####  #",
    "#######  #  #      #",
    "#        #  #####  #",
    "#  #######    #    #",
    "#             #    #",
    "#  #################",
    "#                  #",
    "####  ###########  #",
    "#            #     #",
    "#            #     #",
    "####################"
]
levels.append(level_1)

def setup_maze(level):
    for y in range(len(level)):
        for x in range(len(level[y])):
            sprite = level[y][x]
            if sprite == "#":
                addActor(Wall(), Location(x, y))
            elif sprite == "@":
                addActor(player, Location(x, y))
        

## Main

makeGameGrid(20, 15, 32, Color.black, False, keyPressed = keyCallback)
setTitle("In den Kerkern des Grauens")
setBgColor(Color(154, 205, 50))

player = Player()

setup_maze(levels[0])

show()
doRun()