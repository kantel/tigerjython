# Lotka-Volterra-Gleichung mit dem Euler-Verfahren
# b = Beute
# r = Räuber

from gpanel import *

B0 = 100 # Startwert Beutepopulation
R0 = 45  # Startwert Räuberpopulation

eps1 = 0.5      # Reproduktionsrate der Beute
gamma1 = 0.0333 # Freßrate der Räuber = Sterberate der Beute
eps2 = 1.0      # Sterberate der Räuber
gamma2 = 0.01   # Reproduktionsrate der Räuber

dt = 0.05       # delta time: Zeit- oder Iterationschritte
N = 1000        # N = Zahl der Iterationen

myFont = Font("American Typewriter", Font.PLAIN, 12)

makeGPanel(-100, 1100, -20, 320)
title("Räuber-Beute-System nach Lotka-Volterra (Rot: Beute, Blau: Räuber)")
font(myFont)
drawGrid(0, 1000, 0, 300)

b = B0
r = R0

lineWidth(2)
for n in range(N):
    bNew = b + dt*(eps1*b - gamma1*b*r)
    # rNew = r + dt*(-eps2*r + gamma2*b*r)  # Explizites Euler-Verfahren
    rNew = r + dt*(-eps2*r + gamma2*bNew*r) # Euler-Cromer-Verfahren
    setColor("red")
    setColor("red")
    line(n, b, n + 1, bNew)
    setColor("blue")
    line(n, r, n + 1, rNew)
    b = bNew
    r = rNew