# Lotka-Volterra-Gleichung mit dem Euler-Verfahren
# b = Beute
# r = Räuber

from gpanel import *

# Farbpalette
vaporwave    = ["#94d0ff", "#8795e8", "#966bff", "#ad8cff",
                "#c774e8", "#c774a9", "#ff6ad5", "#ff6a8b",
                "#ff8b8b", "#ffa58b", "#ffde8b", "#cdde8b",
                "#8bde8b", "#20de8b"]

eps1 = 0.5      # Reproduktionsrate der Beute
gamma1 = 0.0333 # Freßrate der Räuber = Sterberate der Beute
eps2 = 1.0      # Sterberate der Räuber
gamma2 = 0.01   # Reproduktionsrate der Räuber

dt = 0.05       # delta time: Zeit- oder Iterationschritte
N = 1000        # N = Zahl der Iterationen

myFont = Font("American Typewriter", Font.PLAIN, 12)

makeGPanel(-200, 1300, -50, 260)
title("Räuber-Beute-System nach Lotka-Volterra (Phasendiagramm)")
bgColor(makeColor("#2b3e50"))
font(myFont)
drawGrid(-50, 1200, -20, 240, makeColor("#fbcff3"))

b = [80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300] # Beutepopulationen
r = [45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210]    # Räuberpopulationen

lineWidth(2)
for i in range(len(b)):
    setColor(makeColor(vaporwave[i%len(b)]))
    for n in range(N):
        bNew = b[i] + dt*(eps1*b[i] - gamma1*b[i]*r[i])
        rNew = r[i] + dt*(-eps2*r[i] + gamma2*bNew*r[i]) # Euler-Cromer-Verfahren        
        line(b[i],r[i], bNew, rNew)
        b[i] = bNew
        r[i] = rNew
print("I did it, Babe!")