# Lotka-Volterra-Gleichung mit dem Runge-Kutta-4-Verfahren
# b = Beute
# r = Räuber

from gpanel import *

B0 = 100 # Startwert Beutepopulation
R0 = 45  # Startwert Räuberpopulation

eps1 = 0.5      # Reproduktionsrate der Beute
gamma1 = 0.0333 # Freßrate der Räuber = Sterberate der Beute
eps2 = 1.0      # Sterberate der Räuber
gamma2 = 0.01   # Reproduktionsrate der Räuber

dt = 0.005
N = 1000

def rk4(f, g, r, b, dt, n):
    """ Runge-Kutta 4 """
    lineWidth(2)
    for i in range(n):
        k1 = dt*f(r, b)
        k2 = dt*f(r + 0.5*k1, b + 0.5*dt)
        k3 = dt*f(r + 05.*k2, b + 0.5*dt)
        k4 = dt*f(r + k3, b + dt)
        bNew = b + (k1 + 2*k2 + 2*k3 + k4)/6.0
        # print(bNew)
        setColor("red")
        line(i - 1, b,  i, bNew)
        k1 = dt*g(r, b)
        k2 = dt*g(r + 0.5*k1, b + 0.5*dt)
        k3 = dt*g(r + 05.*k2, b + 0.5*dt)
        k4 = dt*g(r + k3, b + dt)
        rNew = r + (k1 + 2*k2 + 2*k3 + k4)/6.0
        setColor("blue")
        line(i - 1, r, i, rNew)
        b = bNew
        r = rNew


def f(r, b):
    return(eps1*b - gamma1*b*r)

def g(r, b):
    return(eps1*r + gamma2*b*r)
    


myFont = Font("American Typewriter", Font.PLAIN, 12)

makeGPanel(-100, 1100, -20, 220)
title("Räuber-Beute-System nach Lotka-Volterra (Rot: Beute, Blau: Räuber)")
font(myFont)
drawGrid(0, 1000, 0, 200)

b = B0
r = R0

rk4(f, g, r, b, dt, N)


    
 