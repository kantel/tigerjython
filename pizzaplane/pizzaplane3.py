# Pizza Plane Stage 3: Klasse »Enemy«
# Background Image: »PWL« (https://opengameart.org/content/seamless-desert-background-in-parts)
# Aeroplane: Tappy Plane, Kenney (https://www.kenney.nl/assets/tappy-plane)

from gamegrid import *
from random import randint
import os

WIDTH = 720
HEIGHT = 480

DATAPATH = os.path.join(os.getcwd(), "data")
BGWIDTH    = 1067   # Breite des Hintergrundbildes
BGWIDTH2   = 533    # BGWIDTH//2 (abgerundet)
NO_ENEMIES = 10     # Anzahl der Feinde

class Background(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "desert.png"))
        self.speed = -1
        
    def act(self):
        self.move(self.speed)
        if self.getX() < -BGWIDTH2:
            self.setX(BGWIDTH + BGWIDTH2)

class Plane(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "planered.png"), 3)
        self.timer = 5
        self.updown = 2
        
    def act(self):
        if isKeyPressed(38):    # UP
            if self.getY() > 20:
                self.setY(self.getY() - self.updown)
        if isKeyPressed(40):    # DOWN
            if self.getY() < HEIGHT - 20:
                self.setY(self.getY() + self.updown)
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 5
        self.timer -= 1

class Enemy(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "pizza.png"))
        self.speed = randint(3, 6)
        
    def act(self):
        self.move(-self.speed)
        if self.getX() < -30:
            self.setX(WIDTH + randint(30, 100))
            self.setY(randint(30, HEIGHT - 30))
            self.speed = randint(3, 6)
 
win = makeGameGrid(WIDTH, HEIGHT, 1, Color.GRAY, True)
win.setTitle("Pizza Plane Stage 3: The Attack of the Killer Pizzas")
win.setSimulationPeriod(20)

bg1 = Background()
bg2 = Background()
win.addActor(bg1, Location(BGWIDTH2, 260))
win.addActor(bg2, Location(BGWIDTH + BGWIDTH2, 260))
for _ in range(NO_ENEMIES):
    pizza = Enemy()
    win.addActor(pizza, Location(WIDTH + randint(30, 100), randint(30, HEIGHT - 30)))
redBaron = Plane()
win.addActor(redBaron, Location(70, 200))

win.show()
win.doRun()