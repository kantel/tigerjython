# Pizza Plane Stage 4: Klasse »Missile and Explosion«
# Background Image: »PWL« (https://opengameart.org/content/seamless-desert-background-in-parts)
# Aeroplane: Tappy Plane, Kenney (https://www.kenney.nl/assets/tappy-plane)
# Pizza: Twitter Twemoji (https://twemoji.twitter.com/)
# Missile and Explosion: TigerJython Sprite-Bibliothek 
# (https://www.programmierkonzepte.ch/index.php?inhalt_links=navigation.inc.php&inhalt_mitte=sprites.html)

from gamegrid import *
from random import randint
import os

# -------------------------- Konstanten ---------------------------- #
WIDTH = 720
HEIGHT = 480

DATAPATH = os.path.join(os.getcwd(), "data")
BGWIDTH    = 1067   # Breite des Hintergrundbildes
BGWIDTH2   = 533    # BGWIDTH//2 (abgerundet)
NO_ENEMIES = 20     # Anzahl der Feinde

# -------------------------- Background ---------------------------- #
class Background(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "desert.png"))
        self.speed = -1
        
    def act(self):
        self.move(self.speed)
        if self.getX() < -BGWIDTH2:
            self.setX(BGWIDTH + BGWIDTH2)

# -------------------------- Player Class ---------------------------- #
class Plane(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "planered.png"), 3)
        self.timer = 5
        self.firecount = 0
        self.updown = 2
    
    def fire(self):
        if self.firecount < 0:
            missile = Missile()
            enemyList = win.getActors(Enemy)
            for enemy in enemyList:
                missile.addCollisionActor(enemy)
            win.addActor(missile, Location(self.getX() + 15, self.getY()))
            self.firecount = 15

    def act(self):
        if isKeyPressed(38):    # UP
            if self.getY() > 20:
                self.setY(self.getY() - self.updown)
        if isKeyPressed(40):    # DOWN
            if self.getY() < HEIGHT - 20:
                self.setY(self.getY() + self.updown)
        if isKeyPressed(32):    # SPACE
            self.fire()
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 5
        self.timer -= 1
        self.firecount -= 1

# -------------------------- Missile Class -------------------------- #                
class Missile(Actor):

    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "missile.png"))
        self.speed = 5
                        
    def act(self):
        self.move(self.speed)
        if self.getX() >= WIDTH + 20:
            win.removeActor(self)
    
    def collide(self, actor1, actor2):
        xpos = actor2.getX()
        ypos = actor2.getY()
        win.removeActor(self)
        win.removeActor(actor2)
        hit = Explosion()
        win.addActor(hit, Location(xpos, ypos))
#        if win.getNumberOfActors(Enemy) == 15:
#            win.doPause()       # für Screenshot
        return 0

# -------------------------- Missile Class -------------------------- #

class Explosion(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "explosion.png"))
        self.timer = 5
        
    def act(self):
        self.timer -= 1
        if self.timer <= 0:
            win.removeActor(self)

# -------------------------- Enemy Class ---------------------------- #
class Enemy(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "pizza.png"))
        self.speed = randint(3, 6)
        
    def reset(self):
        self.setLocation(Location(WIDTH + randint(30, 100), randint(30, HEIGHT - 30)))
        self.speed = randint(3, 6)
        
    def act(self):
        self.move(-self.speed)
        if self.getX() < -30:
            self.reset()

# -------------------------- Main Loop ---------------------------- #                        
win = makeGameGrid(WIDTH, HEIGHT, 1, Color.GRAY, True)
win.setTitle("Pizza Plane Stage 4: Shooting the Killer Pizzas")
win.setPosition(1400, 40)
win.setSimulationPeriod(20)

bg1 = Background()
bg2 = Background()
win.addActor(bg1, Location(BGWIDTH2, 260))
win.addActor(bg2, Location(BGWIDTH + BGWIDTH2, 260))
for _ in range(NO_ENEMIES):
    pizza = Enemy()
    win.addActor(pizza, Location(WIDTH + randint(30, 400), randint(30, HEIGHT - 30)))
redBaron = Plane()
win.addActor(redBaron, Location(70, 200))

win.show()
win.doRun()