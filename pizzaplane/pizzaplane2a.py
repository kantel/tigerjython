# Pizza Plane Stage 2: Exkurs Video
# Background Image: »PWL« (https://opengameart.org/content/seamless-desert-background-in-parts)
# Aeroplane: Tappy Plane, Kenney (https://www.kenney.nl/assets/tappy-plane)

from gamegrid import *
import os

WIDTH = 720
HEIGHT = 480

win = makeGameGrid(WIDTH, HEIGHT, 1, Color.GRAY, True)
win.setTitle("Pizza Plane")
win.setSimulationPeriod(20)

DATAPATH = os.path.join(os.getcwd(), "data")
BGWIDTH = 1067   # Breite des Hintergrundbildes
BGWIDTH2 = 533   # BGWIDTH//2 (aufgerundet)

recDone = False
rec = VideoRecorder(getFrame(), os.path.join(DATAPATH, "pizzaplane.mp4"), "720x480")

class Background(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "desert.png"))
        self.speed = -1
        
    def act(self):
        self.move(self.speed)
        rec.captureImage()
        if self.getX() < -BGWIDTH2:
            self.setX(BGWIDTH + BGWIDTH2)
            recDone = True
 
class Plane(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "planered.png"), 3)
        self.timer = 5
        self.updown = 2
        
    def act(self):
        if isKeyPressed(38):    # UP
            if self.getY() > 20:
                self.setY(self.getY() - self.updown)
        if isKeyPressed(40):    # DOWN
            if self.getY() < HEIGHT - 20:
                self.setY(self.getY() + self.updown)
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 5
        self.timer -= 1

bg1 = Background()
bg2 = Background()
win.addActor(bg1, Location(BGWIDTH2, 260))
win.addActor(bg2, Location(BGWIDTH + BGWIDTH2, 260))
redBaron = Plane()
win.addActor(redBaron, Location(50, 200))

win.show()
win.doRun()

if recDone:
    rec.finish()
    print("Aufnahme beendet")