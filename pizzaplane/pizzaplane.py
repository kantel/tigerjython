# Pizza Plane Stage 1: Endless Scrolling Background
# Background Image: »PWL« (https://opengameart.org/content/seamless-desert-background-in-parts)
# Aeroplane: Tappy Plane, Kenney (https://www.kenney.nl/assets/tappy-plane)

from gamegrid import *
import os

win = makeGameGrid(720, 520, 1, Color.GRAY, True)
win.setTitle("Pizza Plane")
win.setSimulationPeriod(20)

DATAPATH = os.path.join(os.getcwd(), "data")
BGWIDTH = 1067   # Breite des Hintergrundbildes
BGWIDTH2 = 533   # BGWIDTH//2 (aufgerundet)

class Background(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "desert.png"))
        self.speed = -1
        
    def act(self):
        self.move(self.speed)
        if self.getX() < -BGWIDTH2:
            self.setX(BGWIDTH + BGWIDTH2)

class Plane(Actor):
    
    def __init__(self):
        Actor.__init__(self, True, os.path.join(DATAPATH, "planered.png"), 3)
        self.timer = 5
        
    def act(self):
        if self.timer == 0:
            self.showNextSprite()
            self.timer = 5
        self.timer -= 1

bg1 = Background()
bg2 = Background()
win.addActor(bg1, Location(BGWIDTH2, 260))
win.addActor(bg2, Location(BGWIDTH + BGWIDTH2, 260))
redBaron = Plane()
win.addActor(redBaron, Location(50, 200))

win.show()
win.doRun()